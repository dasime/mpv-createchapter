# Create EDL

Simple chapter maker with ability to export valid mpv EDL files.

The generated file can be opened directly in mpv without any further processing.

EDL specification: <https://github.com/mpv-player/mpv/blob/master/DOCS/edl-mpv.rst>

Forked from Shinchiro's Create Chapter script: <https://github.com/shinchiro/mpv-createchapter>

## Usage

Place `createedl.lua` file into mpv scripts folder.

## Keybinds

`Shift-c` - Mark edit decision

`Shift-b` - Export EDL file
